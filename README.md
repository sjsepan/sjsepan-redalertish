# RedAlertish Theme

RedAlertish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-redalertish_code.png](./images/sjsepan-redalertish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-redalertish_codium.png](./images/sjsepan-redalertish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-redalertish_codedev.png](./images/sjsepan-redalertish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-redalertish_ads.png](./images/sjsepan-redalertish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-redalertish_theia.png](./images/sjsepan-redalertish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-redalertish_positron.png](./images/sjsepan-redalertish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/10/2025
